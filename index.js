const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [SECTION] MongoDB connection
// Syntax -> mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@zuitt.v06ikbx.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// [SECTION] Mongoose Schemas
// It determines the structure of our documents to be stored in the database
// It acts as our data blueprint and guide


const taskSchema = new mongoose.Schema({
	name : {
		type: String,
		require: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
});

// [SECTION] Models 
const Task = mongoose.model("Task", taskSchema);

// [SECTION] Creation of to do list routes

app.use(express.json());
// allows ur app to read data from forms
app.use(express.urlencoded({extended: true}));

// Creating a new task

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database.
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found");
		}else{
			let newTask= new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New Task created");
				}
			})
		}
	})
})

//START OF ACTIVITY

const userSchema = new mongoose.Schema({
	username : { 
		type: String,
		required: [true, "Username is required"]
	},

	password : { 
		type: String,
		required: [true, "Password is required"]
	}
});


const User = mongoose.model("User", userSchema);

app.post("/signup", (request, response) => {
	User.findOne({username: request.body.username}, (err, result) =>{
		if(result != null && result.username == request.body.username){
			return response.send("Duplicate found");
		}else {
			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			});

			newUser.save((saveErr, saveTask) => {
				if (saveErr) {
					return console.error(saveErr);
				}else{
					return response.status(201).send("New user registered");
				}
			})
		}
	})
})


//END OF ACTIVITY



// Get method
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));






















